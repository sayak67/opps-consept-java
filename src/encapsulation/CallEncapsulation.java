package encapsulation;

public class CallEncapsulation {

	public static void main(String[] args) {
		EncapsulationIntro y =new EncapsulationIntro();
		// dowork  is not ascessable from the object because the method is private
		y.dowork();
	}

}
