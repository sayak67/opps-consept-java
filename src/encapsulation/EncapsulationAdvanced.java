package encapsulation;

public class EncapsulationAdvanced {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Food obj= new Food();
		//obj.name="agent47";
		// you can add validation in getters and setters that any unwanted person not able to change the any values
		obj.setName("sayak");
		// here are using the getters to access the 
		System.out.println(obj.getName());
		
		
		

	}

}

class Food{
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	//protected String name;
	int waight;
	 
	
}

// if you make the food class private then it can not get access from the other class apart from the food class
// if it is protected then it can get access from the classes under the same packages
// if it is public then can get access from any where.
// to change the value of private element in java you have to use getters and setters 