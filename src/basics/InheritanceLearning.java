package basics;

public class InheritanceLearning {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Developer d1= new Developer("sayak",28);
		
		// here i am inherateing the parent class properties 
		d1.occupation();

	}

}

class Developer extends Somebody{

	public Developer(String name, int age) {
		// super keyword is used to call the parent constructor
		super(name, age);
	}
	
}



class Somebody{
	String name;
	int age;
	public Somebody(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	void occupation() {
		System.out.println(name+" "+"is a engineer"+" "+"his age is"+" "+age);
	}
}