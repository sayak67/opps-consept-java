package basics;

public class ConstructoroverLoading {

	public static void main(String[] args) {
		person obj1 =new person(30,100);
		obj1.walk();
		obj1.talk();

	}

}


class person{
	int age;
	int length;
	static int count;
	
	public person(){
		count++;
		System.out.println("constructor is calling"+" "+count+" "+"times");
		
	}
	
	public person(int age,int length) {
		this();
		this.age =age;
		this.length=length;
	}
	
	
	
	
	void walk () {

		System.out.println("a person age of"+age+"can walk"+length+"metars");
	}
	
	void talk() {
		System.out.println("a person can talk");
	}
}