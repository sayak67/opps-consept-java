package basics;

public class MainClass {

	public static void main(String[] args) {
		System.out.println("hello world");
		
// Creating object of a Fish class 		
//		Fish item1= new Fish();
//		item1.fishname= "goldfish";
//		item1.price= 200;
//		item1.food();
//		item1.tanksize();
//		item1.food(10);		
//		System.out.println(item1.fishname + " "+item1.price);
		
		
//creating a another object of the Fish class inside bellow its going to take another space in memory location 		
		Fish item2 = new Fish("discus",2000);
//						
		System.out.println(item2.fishname +" "+item2.price);
		
		
		
		Fish item3= new Fish();
//		item3.fishname="discus";
//		item3.price=2000;		
		
		
// here i calling the constructor 
		System.out.println(Fish.count);
		

	}

}

// how to create a class 
//every class have properties and behaviors
class Fish {
	String fishname;
	int    price;
	// here static means this count veriable belong to this Fish class
	// static is used if you want to make some veriables which are going to fix for this class not going to use by another class so you dont have to make objects to ascess them so static keyword is used
	   
	static int count;
	
	//constructor
	// constructor are not function they are used to build new objects of the class	
	public Fish() {
		count++;
		System.out.println("calling the constarctor :"+count+"times");
	}
	
	// example of perametarized constructor and constructor overloading also
	
	public Fish(String fishname,int price) {
		//this key word is used to call other constructor which are in the class
		//this key word is used to call parameters of the class
		this();
		this.fishname= fishname;
		this.price= price;
		
	}
	
	void food() {
		System.out.println("they can eat every things");
		
	}
	
	void tanksize() {
		System.out.println("anything more then 100 liters");
	}
// here the food method is a example of Polymorphism because we all ready used method named food upside which one is non parameterized again it used here as parameterized	
	void food (int grams) {
		System.out.println("single"+" "+fishname+" "+"can ate"+" "+grams+"grams" );
	}
		
	
}

//constructor
// constructor are not function they are used to build new objects of the class







