package intarface;

public class IntarfaceIntro implements Car,Transformer{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		IntarfaceIntro x = new IntarfaceIntro();
		x.start();
		x.walk();

	}

	@Override
	public void start() {
		System.out.println("car shoul run when it has petrol in tank");
		
	}

	@Override
	public void walk() {
		System.out.println("tranformer can walk also");
		
	}
	
	

}

interface Car{
	// inside the interfaces by default the methods are public and abstract
	// interfaces is the key to get the 100% abstraction
	// Interface can be used to get multiple inheritance
	
	void start();
}


interface Transformer{
	void walk();
}
