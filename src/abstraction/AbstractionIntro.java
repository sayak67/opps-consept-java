 package abstraction;

public class AbstractionIntro {

	public static void main(String[] args) {
		
		Tata x = new Tata();
		x.start();
		Maruti y = new Maruti();
		y.start();
		
		
		//it will through a error because you can not make object of abstract class and abstract method
	//	Car x = new Car();
		

	}

}

class Tata extends Car{

	@Override
	void start() {
		System.out.println("Tata cars are made in india cars");
		
	}
	
}

class Maruti extends Car{

	@Override
	void start() {
		System.out.println("maruti cars are oil efficiant");
		
	}
	
}




abstract class Car{
	String brand;
	int price;
	abstract void start() ;
}


// in real world  car is nothing its map of the product making object of car is useless , 